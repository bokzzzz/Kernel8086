/*
 * IVTEntry.cpp
 *
 *  Created on: Aug 6, 2017
 *      Author: OS1
 */

#include "IVTEntry.h"
#include <DOS.H>
unsigned int orgBp,segReg;
unsigned offsf,segf;
unsigned oldContextOffIVT, oldContextSegIVT;
unsigned  intVec;

IVTEntry::IVTEntry(IVTNo nmb, void (interrupt *func)())
{
	nmbIVT=nmb;
	unsigned adrOff=nmb*4,adrSeg=nmb*4+2;
	unsigned funcOff,funcSeg;
	funcOff=FP_OFF(func);
	funcSeg=FP_SEG(func);
		asm {
			cli
			push es
			push ax
			push di
			mov ax,0
			mov es,ax

			mov di, word ptr adrSeg
			mov ax, word ptr es:di
			mov word ptr oldContextSegIVT, ax
			mov ax, word ptr funcSeg
			mov word ptr es:di, ax

			mov di, word ptr adrOff
			mov ax, word ptr es:di
			mov word ptr oldContextOffIVT, ax
			mov ax, word ptr funcOff
			mov word ptr es:di, ax

			mov di, 184h
			mov ax, word ptr oldContextOffIVT
			mov word ptr es:di, ax
			mov di, word ptr 186h
			mov ax, word ptr oldContextSegIVT
			mov word ptr es:di, ax

			pop di
			pop ax
			pop es
			sti
		}
		offIVT=oldContextOffIVT;
		segIVT=oldContextSegIVT;
}

void IVTEntry::redirect(IVTNo iv)
{
	asm{
		pop segReg
		pop orgBp
		push oldContextOffIVT
		push oldContextSegIVT
		push orgBp
		push segReg
		}
}

IVTEntry::~IVTEntry() {
	unsigned adrOff=nmbIVT*4,adrSeg=nmbIVT*4+2;
	oldContextOffIVT=offIVT;
	oldContextSegIVT=segIVT;
	intVec=nmbIVT;
		asm {
			cli
			push es
			push ax
			push di

			mov ax,0
			mov es,ax

			mov di, word ptr adrSeg
			mov ax, word ptr oldContextSegIVT
			mov word ptr es:di, ax

			mov di, word ptr adrOff
			mov ax, word ptr oldContextOffIVT
			mov word ptr es:di, ax



			pop di
			pop ax
			pop es
			sti
		}
}
