/*
 * mainThrd.h
 *
 *  Created on: Aug 8, 2017
 *      Author: OS1
 */

#ifndef MAINTHRD_H_
#define MAINTHRD_H_
#include "Thread.h"
#include "PCB.h"
extern int userMain(int,char **);
class mainThrd:public Thread {
public:
	mainThrd(int argc,char** argv):Thread(4096,0),argvT(argv),argcT(argc){
		PCB::rnnthrd=this;
		PCB::getPCB()->state=RUNNING;
	}
	int argcT;
	char **argvT;
	void run()
	{
		userMain(argcT,argvT);
	}
};

#endif /* MAINTHRD_H_ */
