/*
 * KernelEv.h
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#ifndef KERNELEV_H_
#define KERNELEV_H_
#include "PCB.h"
#include "Event.h"
class KernelEv {
public:
	KernelEv(IVTNo nmb);
	void wait();
	void signal();
private:
	PCB* pcbEv;
};
#endif /* KERNELEV_H_ */
