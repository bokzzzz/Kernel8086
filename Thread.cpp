/*
 * Thread.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "Thread.h"
#include "PCB.h"
#include "ThrdCol.h"

void Thread::start()
{
	myPCB->state=READY;
	Scheduler::put(myPCB);
	ThreadCollector::buffer.add(myPCB);
}

void Thread::waitToComplete()
{
	if(myPCB !=0 && myPCB->state != ENDED)
	{
	PCB::rnnthrd->myPCB->state=BLOCK;
	myPCB->buf.add(PCB::rnnthrd->myPCB);
	PCB::_contSwitch=YES;
	TickInterrupt();
	}
}
Thread::~Thread(){
	waitToComplete();
}

void Thread::sleep(Time timeToSleep)
{
	PCB::rnnthrd->myPCB->sleep(timeToSleep);
}

Thread::Thread(StackSize stackSize, Time timeSlice)
{
	Kernel_mode;
	myPCB =(PCB*) new PCB(stackSize, timeSlice);
	User_mode;
	myPCB->thisThrd=this;
}

void dispatch() {
PCB::dispatch();
}
