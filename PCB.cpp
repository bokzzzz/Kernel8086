#include "PCB.h"
#include <DOS.H>

PCB::PCB(StackSize stack_s, Time timeslice)
{
	stack_size=stack_s/2;
	stack=new unsigned [stack_size];
	stack[stack_size-1]=0x200;
	stack[stack_size-2]=FP_SEG(wrapper);
	stack[stack_size-3]=FP_OFF(wrapper);
	stSS=FP_SEG(stack+stack_size-12);
	stSP=FP_OFF(stack+stack_size-12);
	timeSlice=tmpTSlice=timeslice;
	timeSleep=0;
	state=NEW;
}

void PCB::wrapper()
{
	rnnthrd->myPCB->state=RUNNING;
	rnnthrd->run();
	rnnthrd->myPCB->buf.checkBlckd();
	rnnthrd->myPCB->state=ENDED;
	_contSwitch=YES;
	TickInterrupt();
}

void PCB::sleep(Time sleep)
{
	rnnthrd->myPCB->timeSleep=sleep;
	rnnthrd->myPCB->state=SLEEP;
	_contSwitch=YES;
	TickInterrupt();
}
PCB::~PCB()
{
	thisThrd->myPCB=0;
	delete[] stack;
}

State PCB::getState() const{
	return state;
}

void PCB::putState(State _state) {
	if(_state >=NEW && _state<=ENDED)
		state=_state;
}

void PCB::dispatch() {
	rnnthrd->myPCB->state=READY;
	_contSwitch=YES;
	TickInterrupt();
}
Time PCB::getTimeSlice() const {
	return timeSlice;
}

Time PCB::getTmpTSlice() const {
	return tmpTSlice;
}

void PCB::setTmpTSlice(Time tmpTSlice) {
	this->tmpTSlice = tmpTSlice;
}

PCB* PCB::getPCB() {
	return rnnthrd->myPCB;
}

