/*
 * KernelEv.cpp
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#include "KernelEv.h"
#include "IVTEntry.h"

KernelEv::KernelEv(IVTNo nmb)
{
	pcbEv=PCB::getPCB();
	IVTEntry::ev[nmb]=this;
}
void KernelEv::wait()
{
	if(pcbEv == PCB::getPCB())
	{
	PCB::getPCB()->putState(BLOCK);
	PCB::_contSwitch=YES;
	TickInterrupt();
	}
}
void KernelEv::signal()
{
	pcbEv->putState(READY);
	Scheduler::put(pcbEv);
}
