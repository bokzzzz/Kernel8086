/*
 * BufList.cpp
 *
 *  Created on: Jul 20, 2017
 *      Author: OS1
 */

#include "BufList.h"
#include "PCB.h"

void BufList::slpTick() {
	Node *itr=head;
while(itr)
{

	if((itr->data->state) == SLEEP){
	if(--(itr->data->timeSleep) ==0)
	{
		itr->data->state=READY;
		Scheduler::put(itr->data);
	}
}
	itr=itr->next;
}
}

void BufList::checkBlckd() {
	while(head)
	{
		head->data->state=READY;
		Scheduler::put(head->data);
		remove(head);
	}
}

void BufList::rmvFinished() {
	Node *itr=head;
	while(itr)
	{
		if((itr->data->state)==ENDED)
		{
			if(itr->next){
			itr=itr->next;
			remove(itr->prev);
			}
			else remove(itr);
		}
		else
		{
			itr=itr->next;
		}
	}
}

int BufList::getCount() {
	return count;
}

void BufList::removeFirst() {
	if(head)
	{
		head->data->putState(READY);
		Scheduler::put(head->data);
		remove(head);
	}
}
