/*
 * KerQueue.h
 *
 *  Created on: Jul 17, 2017
 *      Author: OS1
 */

#ifndef LNKDLST_H_
#define LNKDLST_H_
class PCB;
class LnkdLst
{
public:
	LnkdLst();
	virtual ~LnkdLst();
	void add(PCB* pcb);
protected:
	struct Node
	{
		PCB *data;
		Node *next,*prev;
	};
	int count;
	Node *head,*tail;
public:
	void remove(Node *rmv);
};

#endif /* LNKDLST_H_ */
