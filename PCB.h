/*
 * PCB.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef PCB_H_
#define PCB_H_
#include "Thread.h"
#include "SCHEDULE.H"
#include "BufList.h"

#define Kernel_mode asm cli
#define User_mode asm sti

typedef enum {
	NEW,READY,SLEEP,BLOCK,RUNNING,ENDED
}State;
typedef enum{ NO,YES}ContextSwith;

class PCB
{
public:
	PCB(StackSize stk, Time tslc);
	static void wrapper();
	static void sleep(Time sleep);
	~PCB();
	friend class Thread;
	friend class BufList;
	friend class KernelSem;
	friend class KernelEv;
	friend class mainThrd;
	friend void interrupt TickInterrupt();
	friend int  main(int,char**);
	State getState()const;
	void putState(State _state);
	static void dispatch();
	Time getTimeSlice() const ;
	Time getTmpTSlice() const ;
	void setTmpTSlice(Time tmpTSlice);
	static PCB* getPCB();
	static ContextSwith _contSwitch;
private:
	unsigned *stack;
	unsigned stSP,stSS;
	StackSize stack_size;
	Time timeSleep;
	Time timeSlice;
	Time tmpTSlice;
	State state;
	static Thread *rnnthrd;
	Thread *thisThrd;
	BufList buf;

};

#endif /* PCB_H_ */
