/*
 * Semaphore.h
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#ifndef SEMAPHOR_H_
#define SEMAPHOR_H_

class KernelSem;
class Semaphore {
public:
	Semaphore (int init=1);
	virtual ~Semaphore ();
	virtual void wait ();
	virtual void signal();
	int val () const;
	// Returns the current value of the semaphore
	private:
	KernelSem* myImpl;
};

#endif /* SEMAPHOR_H_ */
