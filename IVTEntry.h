/*
 * IVTEntry.h
 *
 *  Created on: Aug 6, 2017
 *      Author: OS1
 */

#ifndef IVTENTRY_H_
#define IVTENTRY_H_
#include "KernelEv.h"

class IVTEntry {
public:
	IVTEntry(IVTNo nmb,void (interrupt *func)());
	~IVTEntry();
	static KernelEv *ev[256];
	static void redirect(IVTNo ivtNMB);
	unsigned offIVT,segIVT;
	IVTNo nmbIVT;
};
#define PREPAREENTRY(NMB, YN)		\
	void interrupt intFunc##NMB()	\
{									\
	IVTEntry::ev[NMB]->signal();	\
	if(YN==1)						\
	{								\
		IVTEntry::redirect(NMB);	\
	}								\
}									\
IVTEntry ivt##NMB(NMB,intFunc##NMB);
#endif /* IVTENTRY_H_ */
