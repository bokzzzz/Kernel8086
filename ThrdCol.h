/*
 * ThrdCol.h
 *
 *  Created on: Jul 20, 2017
 *      Author: OS1
 */

#ifndef THRDCOL_H_
#define THRDCOL_H_
#include "Thread.h"
#include "BufList.h"

class ThreadCollector:public Thread {
public:
	ThreadCollector():Thread(4096,0){}
	static BufList buffer;
	void run();
};

#endif /* THRDCOL_H_ */
