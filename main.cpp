/*
 * main.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */
#include "stdlib.h"
#include "Intrrpt.h"
#include "BufList.h"
#include "PCB.h"
#include "ThrdCol.h"
#include "mainThrd.h"
#include "IVTEntry.h"
#include "wtThred.h"
volatile unsigned oldContextOff, oldContextSeg;
volatile unsigned adrFree = 0x60;
volatile unsigned contxVec=0x08;
void InitNewContext() {

	unsigned adrOff=contxVec*4,adrSeg=contxVec*4+2;
	unsigned empOff=adrFree*4;
	unsigned empSeg=adrFree*4+2;
	asm {
			cli
			push es
			push ax
			push di
			mov ax,0
			mov es,ax

			mov di, word ptr adrSeg
			mov ax, word ptr es:di
			mov word ptr oldContextSeg, ax
			mov word ptr es:di, seg TickInterrupt

			mov di, word ptr adrOff
			mov ax, word ptr es:di
			mov word ptr oldContextOff, ax
			mov word ptr es:di, offset TickInterrupt

			mov di, word ptr empOff
			mov ax, word ptr oldContextOff
			mov word ptr es:di, ax
			mov di, word ptr empSeg
			mov ax, word ptr oldContextSeg
			mov word ptr es:di, ax

			pop di
			pop ax
			pop es
			sti
		}
}


void rtrnOldContext() {
	unsigned adrOff=contxVec*4,adrSeg=contxVec*4+2;

	asm {
		cli
		push es
		push ax
		push di

		mov ax,0
		mov es,ax

		mov di, word ptr adrSeg
		mov ax, word ptr oldContextSeg
		mov word ptr es:di, ax

		mov di, word ptr adrOff
		mov ax, word ptr oldContextOff
		mov word ptr es:di, ax

		pop di
		pop ax
		pop es

		mov al,20h
		out 20h,al
		sti
	}
}
ContextSwith PCB::_contSwitch=NO;
BufList ThreadCollector::buffer;
Thread *PCB::rnnthrd;
KernelEv * IVTEntry::ev[256];
int main(int argc,char *argv[]){
	ThreadCollector *col=new ThreadCollector;
	col->start();
	int tr=atoi(argv[2]);
	wtThread *wt=new wtThread(tr);
	wt->start();
	mainThrd *mn=new mainThrd(argc,argv);
	InitNewContext();
	mn->run();
	rtrnOldContext();
	return 0;
}



