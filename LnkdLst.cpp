/*
 * KerQueue.cpp
 *
 *  Created on: Jul 17, 2017
 *      Author: OS1
 */

#include "LnkdLst.h"
#include "PCB.h"
#include "SCHEDULE.H"
LnkdLst::LnkdLst()
{
	head=tail=0;
	count=0;
}

void LnkdLst::add(PCB* pcb)
{
	Kernel_mode;
	Node* node= new Node();
	User_mode;
	node->data=pcb;
	node->next=0;
	node->prev=tail;
	count++;
	if(!head)
	{
		head=tail=node;
	}
	else
	{
		tail->next=node;
		tail=node;
	}
}

void LnkdLst::remove(Node *rmv)
{
	if(rmv==head || rmv==tail){
		if(head==tail){
			head=tail=0;
		}
		else if(head==rmv)
	{
		rmv->next->prev=0;
		head=rmv->next;
	}
		else
	{
	rmv->prev->next=0;
	tail=rmv->prev;
	}
	}
	else
	{
		rmv->prev->next=rmv->next;
		rmv->next->prev=rmv->prev;
	}
	count--;
	asm cli;
	delete rmv;
	asm sti;
}

LnkdLst::~LnkdLst() {
}
