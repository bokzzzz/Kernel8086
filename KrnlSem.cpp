/*
 * KernelSem.cpp
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#include "KrnlSem.h"
#include "PCB.h"

KernelSem::KernelSem(int n):value(n) {
}

void KernelSem::wait() {
	if(--value <0)
	{
		PCB::getPCB()->putState(BLOCK);
		blocked.add(PCB::getPCB());
		PCB::_contSwitch=YES;
		TickInterrupt();
	}
}

void KernelSem::signal() {
	if(++value <= 0)
	{
		blocked.removeFirst();
	}
}
int KernelSem::getValue() {
	return value;
}
