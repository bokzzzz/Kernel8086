/*
 * KernelSem.h
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#ifndef KRNLSEM_H_
#define KRNLSEM_H_
#include "BufList.h"
class KernelSem {
public:
	KernelSem(int n);
	void wait();
	void signal();
	int getValue();
	friend class Semaphore;
private:
	int value;
	BufList blocked;
};

#endif /* KRNLSEM_H_ */
