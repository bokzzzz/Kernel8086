/*
 * Interrupt.cpp
 *
 *  Created on: Aug 11, 2017
 *      Author: OS1
 */

#include "Intrrpt.h"
#include "PCB.h"
#include "SCHEDULE.H"
#include "ThrdCol.h"

unsigned tmpSP,tmpSS;
void interrupt TickInterrupt()
{
	if(PCB::_contSwitch == NO)
	{
		ThreadCollector::buffer.slpTick();
		PCB::getPCB()->tmpTSlice--;
		asm int 60h;
	}
	if(PCB::getPCB()->tmpTSlice == 0 || PCB::_contSwitch == YES)
	{
		asm{
			mov tmpSP,sp
			mov tmpSS,ss
		}
		PCB::_contSwitch=NO;
		PCB::getPCB()->stSP=tmpSP;
		PCB::getPCB()->stSS=tmpSS;
		if(PCB::getPCB()->state == READY ||
				PCB::getPCB()->state == RUNNING)
		{
		PCB::getPCB()->state = READY;
		Scheduler::put(PCB::getPCB());
		}
		PCB::getPCB()->tmpTSlice=PCB::getPCB()->timeSlice;
		PCB::rnnthrd=Scheduler::get()->thisThrd;
		PCB::getPCB()->state=RUNNING;
		tmpSP=PCB::getPCB()->stSP;
		tmpSS=PCB::getPCB()->stSS;
		asm{
			mov sp,tmpSP
			mov ss,tmpSS
		}
	}
}
