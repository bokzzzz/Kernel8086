/*
 * Semaphore.cpp
 *
 *  Created on: Jul 22, 2017
 *      Author: OS1
 */

#include "KrnlSem.h"
#include "semaphor.h"
#include "PCB.h"

Semaphore::Semaphore(int init){
	Kernel_mode;
	myImpl=new KernelSem(init);
	User_mode;
}


Semaphore::~Semaphore() {
	Kernel_mode;
	delete myImpl;
	User_mode;
}

void Semaphore::wait() {
	myImpl->wait();
}

void Semaphore::signal() {
	myImpl->signal();
}

int Semaphore::val() const {
	return myImpl->getValue();
}
