/*
 * Event.cpp
 *
 *  Created on: Aug 6, 2017
 *      Author: OS1
 */

#include "Event.h"
#include "kernelEv.h"
#include "IVTEntry.h"

Event::Event(IVTNo ivtNo) {
	Kernel_mode;
	myImpl = new KernelEv(ivtNo);
	User_mode;
}

Event::~Event() {
	Kernel_mode;
	delete myImpl;
	User_mode;
}

void Event::wait() {
	myImpl->wait();
}

void Event::signal() {
	myImpl->signal();
}
