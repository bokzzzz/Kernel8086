/*
 * BufList.h
 *
 *  Created on: Jul 20, 2017
 *      Author: OS1
 */

#ifndef BUFLIST_H_
#define BUFLIST_H_

#include "LnkdLst.h"

class BufList: public LnkdLst{
public:
	void slpTick();
	void checkBlckd();
	void rmvFinished();
	int getCount();
	void removeFirst();
};

#endif /* BUFLIST_H_ */
